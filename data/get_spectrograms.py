# -*- coding: utf-8 -*-

import math
import os.path
import torch
import torchaudio
import torchaudio.transforms as transforms

def soundToSpectrogram(path, sample_rate = 16000, duration = 30, nb_mels = 128) :
    r"""Return the Mel-spectrograms of consecutive parts of an audio file.

    Args :
        path (str): the path to the file.
        sample_rate (int, optional) : the sample rate in Hz to use when getting the spectrogram. Default : 16000 Hz
        duration (int, optional) : the number of seconds each part should last. Default : 30s
        nb_mels (int, optional) : the number of frequencies in the resulting spectrograms. Default : 128
    Returns :
        torch.Tensor : Mel frequency spectrogram of size (2, 128, time)"""

    # Load the recording
    waveform, orig_freq = torchaudio.load(path)

    # Convert to mono
    waveform = waveform.mean(dim = 0, keepdim = True)

    # Resample
    resample = transforms.Resample(orig_freq = orig_freq, new_freq = sample_rate)
    waveform = resample(waveform)

    # Cut the recording and compute the Mel-spectrogram
    nb_frames = duration * sample_rate
    spectrograms = []
    
    get_melspectrogram = transforms.MelSpectrogram(sample_rate = sample_rate,
                                                  n_mels = nb_mels)
    
    while waveform.size()[1] >= nb_frames :
        sample = waveform[:,:nb_frames]
        waveform = waveform[:,nb_frames:]
        spectrogram = get_melspectrogram(sample)
        spectrograms.append(spectrogram)
        
    return spectrograms


def getSpectrograms(dir_source, dir_target,
                    sample_rate = 16000,
                    overwrite = False,
                    duration = 30,
                    nb_mels = 128,
                    to_ignore = set()) :
    r"""Compute the Mel spectrograms of all the sound files and write them to memory.

    Args :
        dir_source (str) : the directory where the sound files are. 
        dir_target (str) : the directory to save the the spectrograms to.
        sample_rate (int, optional) : the sample rate in Hz to use when getting the spectrogram. Default : 16000 Hz
        overwrite (bool, optional) : whether to compute all spectrograms again. Default : False
        duration (int, optional) : the number of seconds the recording should last. Default : 30s
        nb_mels (int, optional) : the number of frequencies in the resulting spectrograms. Default : 128
        to_ignore (set, optional) : the set of titles of recordings that should not be downloaded."""

    for filename in os.listdir(dir_source) :
        title, _ = os.path.splitext(filename)
        path_source = os.path.join(dir_source, filename)
        path_target = os.path.join(dir_target, title + '-{part}.pt')

        print('Computing Mel-spectrograms for {filename} ...'.format(filename = filename))
        if (overwrite or not(os.path.isfile(path_target))) and not(title in to_ignore) :
            try :
                spectrograms = soundToSpectrogram(path_source,
                                                  sample_rate = sample_rate,
                                                  duration = duration,
                                                  nb_mels = nb_mels)
                for i, spectrogram in enumerate(spectrograms) :
                    torch.save(spectrogram, path_target.format(part = i + 1))
                print('Success.')
            except Exception as exception :
                print('Failure : {exception}.'.format(exception = exception))
        else :
            print('Mel-spectrogram already computed.')
            
if __name__ == '__main__' :
    getSpectrograms('sounds', 'spectrograms')
